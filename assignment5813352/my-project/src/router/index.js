import Vue from 'vue'
import Router from 'vue-router'
import NewUser from '@/components/NewUser'
import UserList from '@/components/UserList'
import UpdateUser from '@/components/UpdateUser'
import Login from '@/components/Login'

import SuiVue from 'semantic-ui-vue'
Vue.use(SuiVue)


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/NewUser',
      name: 'NewUser',
      component: NewUser
    },
    {
      path: '/UserList',
      name: 'UserList',
      component: UserList
    },
    {
      path: '/UpdateUser/:userId',
      name: 'UpdateUser',
      component: UpdateUser
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '*',
      redirect: '/Login'
    },
    {
      path : '/Login',
      name :'Login',
      component:Login
    }
  ]
})
