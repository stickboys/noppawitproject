var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var jwt = require('jsonwebtoken')


app = express()
port = process.env.PROT || 8081
User = require('./api/models/userListModel')
Users = require('./api/models/userListModel')
mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/Userdb',function(error){
    if(error) throw error
    console.log('Successfully connected')
})
app.use(bodyParser.urlencoded({
    extended: true
})) 
app.use(bodyParser.json())


var routes = require('./api/routes/userListRoutes')
routes(app)


app.listen(port)
console.log('Server start on : '+ port)
