'use strict'
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var UserSchema = new Schema({ 
    contactid:{
        type: String,
        Required: true

    },
    firstname: {
        type: String,
        Required: true
    },
    lastname: {
        type: String,
        Required: true
    },
    mobile: {
        type: String,
        Required: true
    },
    
    email: {
        type: String,
    },
    facebook: {
        type: String,
       
    },
    image: {
        type: String,

    }

    
})

var UserpassSchema = new Schema({
    username:{
        type: String,
        Required: true
    },
    password:{
        type: String,
        Required: true
    }

})
module.exports = mongoose.model('UserPass',UserpassSchema)


module.exports = mongoose.model('Users', UserSchema)